import React from "react";
import logo from "./logo.svg";
import "./App.css";

const App: React.FC = () => {
  return (
    <div className="app">
      <header className="app-header">
        <nav className="app-bar">
          <img src="" alt="logo" />
          <div className="nav-links">
            <a href="#">Work</a>
            <a href="#">Contact</a>
          </div>
        </nav>

        <section className="hero">
          <h2>
            I'm a person, a website and mobile developer living in Virginia,
            currently working for X company.
          </h2>
        </section>

        <section className="hero-work">
          <div className="work-wrapper">
            <div className="label">Currently:</div>
            <div className="work-container">
              <img src="#" />
              <img src="#" />
            </div>
          </div>
          <div className="work-wrapper">
            <div className="label">Previously:</div>
            <div className="work-container">
              <img src="#" />
              <img src="#" />
            </div>
          </div>
        </section>
      </header>

      <section className="additional-work">
        <h1>Additional Work</h1>
        <div className="add-work-row">
          <div className="label">CompanyName</div>
          <div className="details">
            <div>Development, Design, UX</div>
            <div>CompanyName</div>
            <div>YEAR</div>
          </div>
        </div>
        <div className="add-work-row">
          <div className="label">CompanyName</div>
          <div className="details">
            <div>Development, Design, UX</div>
            <div>CompanyName</div>
            <div>YEAR</div>
          </div>
        </div>
        <div className="add-work-row">
          <div className="label">CompanyName</div>
          <div className="details">
            <div>Development, Design, UX</div>
            <div>CompanyName</div>
            <div>YEAR</div>
          </div>
        </div>
        <div className="add-work-row">
          <div className="label">CompanyName</div>
          <div className="details">
            <div>Development, Design, UX</div>
            <div>CompanyName</div>
            <div>YEAR</div>
          </div>
        </div>
      </section>

      <footer>
        <div className="footer-subtext">
          Looking to start a project? Let's talk
        </div>
        <div className="footer-links">
          <a href="#">Github</a>
          <a href="#">CodePen</a>
          <a href="#">LinkedIn</a>
          <a href="#">Instagram</a>
          <a href="#">Email</a>
        </div>
      </footer>
    </div>
  );
};

export default App;
